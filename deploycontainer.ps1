# If running locally, get these values from the azure devops pipeline variables
# $env:ARM_CLIENT_ID="NEEDED_TO_RUN_LOCALLY"
# $env:ARM_CLIENT_SECRET="NEEDED_TO_RUN_LOCALLY"

# Build the image
iex "docker build -t tingo ."

# Login to docker with ACR creds
iex "docker login tsstingo.azurecr.io --username $env:ARM_CLIENT_ID --password $env:ARM_CLIENT_SECRET"

# Tag the image
iex "docker tag tingo tsstingo.azurecr.io/tingo"

# Push the image to ACR
iex "docker push tsstingo.azurecr.io/tingo"

# Eventually we might want these names to come from a variable or something
iex "az webapp restart -n tsstingo -g tss_tingo"