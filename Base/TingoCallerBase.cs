﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Tingo.Data;
using Tingo.ViewModels;

namespace Tingo.Base
{
    public class TingoCallerBase : ComponentBase
    {


        [Inject]
        public  TingoService tingoService { get; set; }

        public CallerCardTile tile = new CallerCardTile();
        public CallerCardTile[] _buttonArray = new CallerCardTile[75];
        public List<int> _numberSelectedList = new List<int>();


        protected override void OnInitialized()
        {
            for (var i = 0; i < 75; i++)
            {
                _buttonArray[i] = new CallerCardTile(){Number = i + 1};
                if (tingoService.GetRolledNumbers().Contains(_buttonArray[i].Number))
                    SelectedBall(_buttonArray[i]);
            }
        }

        public void RollBall()
        {
            var numberRolled = tingoService.Roll();
            if (numberRolled == null) return;
            _numberSelectedList.Add(numberRolled.Value);

            var number = numberRolled.Value - 1;
            SelectedBall(_buttonArray[number]);
        }


        private void SelectedBall(CallerCardTile rolledBall)
        {
            rolledBall.Selected = true;
            rolledBall.BackgroundColor = "Black";
            rolledBall.TextColor = "White";
        }
    }
}
