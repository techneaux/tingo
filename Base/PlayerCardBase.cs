﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tingo.Data;

namespace Tingo.Pages
{
    public class PlayerCardBase : ComponentBase
    {
        [Inject]
        public ITingoService TingoService { get; set; }

        //public TingoService _tingoService = new TingoService();        
        public Player player = new Player();
        public bool showNameInput = true, showCard = false, showInvalidName = false, showHasTingo = false;        
        public List<int> selectedTiles = new List<int>();

        public void HandleValidSubmit()
        {
           
            showInvalidName = false;
            if (TingoService.AddPlayer(player))
            {
                showCard = true; 
                showNameInput = false;
            }
            else
            {
                showInvalidName = true;
            }            
        }
        public void HandleInvalidSubmit()
        {
            showInvalidName = false;
            showCard = false;
        }

        public void SelectTile(MouseEventArgs e, int cell)
        {
            if (selectedTiles.Contains(cell)) selectedTiles.Remove(cell);
            else selectedTiles.Add(cell);
            var rolledNumnbers = TingoService.GetRolledNumbers();
            if(selectedTiles.All(x=> rolledNumnbers.Contains(x)) && player.Card.CalculateTingo(rolledNumnbers)) showHasTingo = true;            
        }
    }
}
