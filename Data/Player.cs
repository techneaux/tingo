﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Tingo.Data
{
    public class Player
    {
        public Player()
        {
            Card = new TingoCard();
        }

        public Player(string name)
        {
            Name = name;
            Card = new TingoCard();
        }

        [Required]
        [MaxLength(10, ErrorMessage = "Name too short or too long")]
        [MinLength(3, ErrorMessage = "Name too short")]
        public string Name { get; set; }
        
        public TingoCard Card { get; set; }

        public bool HasTingo { get; set; }
    }
}
