﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tingo.Data
{
    public class TingoCard
    {
        private const int NUMBER_RANGE = 15;
        private const int GRID_SIZE = 5;
        private const int FREE_CELL_ROW = 2;
        private const int FREE_CELL_COLUMN = 2;
        private const int FREE_CELL = -1;

        public TingoCard()
        {
            Randomize();
        }

        public int[][] Cells;

        public bool CalculateTingo(List<int> rolledNumbers)
        {
            //rolledNumbers.Clear();
            //rolledNumbers.Add(33);
            //rolledNumbers.Add(34);
            //rolledNumbers.Add(35);
            //rolledNumbers.Add(36);
            //rolledNumbers.Add(37);

            ////Testing
            //Cells[0][4] = 33;
            //Cells[1][3] = 34;
            ////Cells[2][2] = -1;
            //Cells[3][1] = 36;
            //Cells[4][1] = 37;

            //Row victory or row free cell victory
            if (Cells.Any(row => row.Where(column => rolledNumbers.Contains(column)).Count() == GRID_SIZE) ||
                Cells[FREE_CELL_ROW].Where(column => rolledNumbers.Contains(column)).Count() == GRID_SIZE - 1)
                return true;

            //Column victory
            for(int columnIndex = 0; columnIndex < GRID_SIZE; columnIndex ++)
            {
                List<int> values = new List<int>();

                for (int rowIndex = 0; rowIndex < GRID_SIZE; rowIndex++)
                    values.Add(Cells[rowIndex][columnIndex]);
            
                //Check for free cell victory or normal victory
                if(values.Where(v => rolledNumbers.Contains(v)).Count() == GRID_SIZE ||
                   (columnIndex == FREE_CELL_COLUMN && values.Where(v => rolledNumbers.Contains(v)).Count() == GRID_SIZE - 1))
                    return true;                    
            }

            //Diagonal victory from 0,0 to 4,4
            for (int i = 0; i < GRID_SIZE; i ++)
            {
                if (!rolledNumbers.Contains(Cells[i][i]) && Cells[i][i] != FREE_CELL)
                    break;

                if (i == GRID_SIZE-1)
                    return true;
            }

            //Diagonal victory from 0,4 to 4,0
            for (int i = 0; i < GRID_SIZE; i++)
            {
                if (!rolledNumbers.Contains(Cells[i][GRID_SIZE - (i+1)]) && Cells[i][GRID_SIZE - (i+1)] != FREE_CELL)
                    break;

                if (i == GRID_SIZE-1)
                    return true;
            }

            return false;
        }

        public void Reset()
        {
            Cells = new int[GRID_SIZE][];

            for (int i = 0; i < GRID_SIZE; i++)
                Cells[i] = new int[GRID_SIZE];


            //Free cell
            Cells[FREE_CELL_ROW][FREE_CELL_COLUMN] = FREE_CELL;
        }

        public void Randomize()
        {
            Reset();
            var r = new Random();

            for (int columnIndex = 0; columnIndex < GRID_SIZE; columnIndex ++)
            {
                List<int> chosen = new List<int>();
                int min = (columnIndex * NUMBER_RANGE) + 1;
                int max = (columnIndex + 1) * NUMBER_RANGE;
                List<int> picked = new List<int>() { 0 };

                for (int rowIndex = 0; rowIndex < GRID_SIZE; rowIndex ++)
                {
                    if (rowIndex == FREE_CELL_ROW && columnIndex == FREE_CELL_COLUMN)
                        continue;//Ignore the free cell

                    while(picked.Contains(Cells[rowIndex][columnIndex]))
                    {
                        Cells[rowIndex][columnIndex] = r.Next(min, max);
                    }

                    picked.Add(Cells[rowIndex][columnIndex]);
                }
            }
        }
    }
}
