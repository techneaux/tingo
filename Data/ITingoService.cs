﻿using System.Collections.Generic;

namespace Tingo.Data
{
    public interface ITingoService
    {

        bool AddPlayer(Player player);
        void AddPlayer(string name);
        List<int> GetRolledNumbers();
        void PlayAgain();
        void RemovePlayer(string name);
        void ResetGame();
        int? Roll();

    }
}