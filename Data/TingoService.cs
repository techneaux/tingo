﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tingo.Data
{
    public class TingoService : ITingoService
    {
        public bool IsLoaded = true;

        public static List<Player> Players = new List<Player>();

        private static readonly List<int> RolledNumbers = new List<int>();


        private void RandomizePlayerCards()
        {
            foreach (var player in Players)
            {
                if (player.Card == null)
                    player.Card = new TingoCard();

                player.Card.Randomize();
            }
        }


        public List<int> GetRolledNumbers() => RolledNumbers;
        public void PlayAgain()
        {
            RolledNumbers.Clear();
            RandomizePlayerCards();
        }

        public void ResetGame()
        {
            Players.Clear();
            RolledNumbers.Clear();
        }

        public int? Roll()
        {
            if (RolledNumbers.Count >= 75) return null;
            var r = new Random();

            int newVal;
            do
            {
                newVal = r.Next(1, 76);
            } while (newVal == 0 || RolledNumbers.Contains(newVal));

            RolledNumbers.Add(newVal);

            foreach (var player in Players)
            {
                player.HasTingo = player.Card.CalculateTingo(RolledNumbers);
            }

            return newVal;

        }

        public void AddPlayer(string name)
        {
            if (!Players.Any(p => p.Name == name))
                Players.Add(new Player(name));
        }

        public void RemovePlayer(string name)
        {
            Players.RemoveAll(p => p.Name == name);
        }

        //Added by Byron
        public bool AddPlayer(Player player)
        {
            if (!Players.Any(p => p.Name == player.Name))
            {
                Players.Add(player);
                return true;
            }
            return false;

        }
    }
}
