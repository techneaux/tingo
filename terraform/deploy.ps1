# If running locally, get these values from the azure devops pipeline variables
# $env:ARM_CLIENT_ID="NEEDED_TO_RUN_LOCALLY"
# $env:ARM_CLIENT_SECRET="NEEDED_TO_RUN_LOCALLY"
# $env:ARM_TENANT_ID="NEEDED_TO_RUN_LOCALLY"

$TF_STATE_STORAGE_ACCOUNT_NAME="tssdevopsstorage"
$TF_STATE_CONTAINER_NAME="tsstingodevopsstate"
$KEYVAULT_NAME="tssdevopskeyvault"
$STORAGE_KEY="tfstate-storage-key"


# Login to azure using service principle
iex "az login --service-principal --username $env:ARM_CLIENT_ID --password $env:ARM_CLIENT_SECRET --tenant $env:ARM_TENANT_ID"

# Initialize the terraform backend in the cloud
iex "terraform init -backend-config=""storage_account_name=$TF_STATE_STORAGE_ACCOUNT_NAME"" -backend-config=""container_name=$TF_STATE_CONTAINER_NAME"" -backend-config=""access_key=$(az keyvault secret show --name $STORAGE_KEY --vault-name $KEYVAULT_NAME --query value -o tsv)"" -backend-config=""key=terraform-ref-architecture-tfstate"""

# [TESTING ONLY] Use plan to test things without changing cloud resources. Comment the apply line.
# iex "terraform plan -var acr_user=$env:ARM_CLIENT_ID -var acr_password=$env:ARM_CLIENT_SECRET"

# [TESTING ONLY] Destroy will... destroy everything. Useful for cleaning botched deployment. Comment the apply line.
# iex "terraform destroy"

# Apply will actually make the changes
iex "terraform apply -auto-approve -var acr_user=$env:ARM_CLIENT_ID -var acr_password=$env:ARM_CLIENT_SECRET"
