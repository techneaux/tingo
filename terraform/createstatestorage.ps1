$LOCATION="eastus"
$COMMON_RESOURCE_GROUP_NAME="tss_devops_rg"
$TF_STATE_STORAGE_ACCOUNT_NAME="tssdevopsstorage"
$TF_STATE_CONTAINER_NAME="tsstingodevopsstate"
$KEYVAULT_NAME="tssdevopskeyvault"
$STORAGE_KEY="tfstate-storage-key"

Write-Output "Don't forget to do an AZ login first."

# Create the resource group
Write-Output "Creating $COMMON_RESOURCE_GROUP_NAME resource group..."
iex "az group create -n $COMMON_RESOURCE_GROUP_NAME -l $LOCATION"

Write-Output "Resource group $COMMON_RESOURCE_GROUP_NAME created."

# Create the storage account
Write-Output "Creating $TF_STATE_STORAGE_ACCOUNT_NAME storage account..."
iex "az storage account create -g $COMMON_RESOURCE_GROUP_NAME -l $LOCATION --name $TF_STATE_STORAGE_ACCOUNT_NAME --sku Standard_LRS --encryption-services blob"

Write-Output "Storage account $TF_STATE_STORAGE_ACCOUNT_NAME created."

# Retrieve the storage account key
Write-Output "Retrieving storage account key..."
$ACCOUNT_KEY = iex "az storage account keys list --resource-group $COMMON_RESOURCE_GROUP_NAME --account-name $TF_STATE_STORAGE_ACCOUNT_NAME --query [0].value -o tsv"

Write-Output "Storage account key retrieved."

# Create a storage container (for the Terraform State)
Write-Output "Creating $TF_STATE_CONTAINER_NAME storage container..."
iex "az storage container create --name $TF_STATE_CONTAINER_NAME --account-name $TF_STATE_STORAGE_ACCOUNT_NAME --account-key $ACCOUNT_KEY"

Write-Output "Storage container $TF_STATE_CONTAINER_NAME created."

# Create an Azure KeyVault
Write-Output "Creating $KEYVAULT_NAME key vault..."
iex "az keyvault create -g $COMMON_RESOURCE_GROUP_NAME -l $LOCATION --name $KEYVAULT_NAME"

Write-Output "Key vault $KEYVAULT_NAME created."

# Store the Terraform State Storage Key into KeyVault
Write-Output "Store storage access key into key vault secret..."
iex "az keyvault secret set --name tfstate-storage-key --value $ACCOUNT_KEY --vault-name $KEYVAULT_NAME"

Write-Output "Key vault secret created."

# Display information
Write-Output "Azure Storage Account and KeyVault have been created."
Write-Output "Run the following command to initialize Terraform to store its state into Azure Storage:"
Write-Output "terraform init -backend-config=\"storage_account_name=$TF_STATE_STORAGE_ACCOUNT_NAME\" -backend-config=\"container_name=$TF_STATE_CONTAINER_NAME\" -backend-config=\"access_key=\$(az keyvault secret show --name $STORAGE_KEY --vault-name $KEYVAULT_NAME --query value -o tsv)\" -backend-config=\"key=terraform-ref-architecture-tfstate\""