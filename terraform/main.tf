provider "azurerm" {
  features{}
}

terraform {
  backend "azurerm" {}
}

variable "rg_name" {
  description = "Name of the resource group"
  default     = "tss_tingo"
}

variable "az_location" {
  description = "Azure location."
  default = "eastus"
}

variable "sp_name" {
  description = "Azure service plan name."
  default = "tsstingo"
}

variable "acr_name" {
  description = "Azure container registry name."
  default = "tsstingo"
}

variable "as_name" {
  description = "Azure app service name."
  default = "tsstingo"
}

variable "container_name" {
  description = "Name of the container"
  default = "tingo"
}

variable "acr_user" {
  description = "Service principle name. Used for ACR access."
}

variable "acr_password" {
  description = "Service principle password. Used for ACR access."
}

# Related docs: 
# https://gaunacode.com/azure-container-registry-and-aks-with-terraform
# https://www.merrell.dev/posts/2018/09/28/azure-web-apps-for-containers-using-terraform/

resource "azurerm_resource_group" "rg" {
  name     = var.rg_name
  location = var.az_location
}

resource "azurerm_container_registry" "acr" {
  name                     = var.acr_name
  resource_group_name      = azurerm_resource_group.rg.name
  location                 = azurerm_resource_group.rg.location
  sku                      = "Basic"
  admin_enabled            = true
}

resource "azurerm_app_service_plan" "sp" {
  name                = var.sp_name
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  kind                = "Linux"
  reserved            = true # Mandatory for Linux plans?
  
  sku {
    tier = "Basic"
    size = "B1"
  }
}

resource "azurerm_app_service" "container" {
  name                = var.as_name
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  app_service_plan_id = azurerm_app_service_plan.sp.id

  app_settings = {
    WEBSITES_ENABLE_APP_SERVICE_STORAGE = false

    # Settings for private Container Registires 
    # This will use the same service principle that the terraform is using
    DOCKER_REGISTRY_SERVER_URL      = "https://${azurerm_container_registry.acr.name}.azurecr.io"
    DOCKER_REGISTRY_SERVER_USERNAME = var.acr_user
    DOCKER_REGISTRY_SERVER_PASSWORD = var.acr_password
  }

  # Configure Docker Image to load on start
  site_config {
    linux_fx_version = "DOCKER|${azurerm_container_registry.acr.name}.azurecr.io/${var.container_name}:latest"
    always_on        = "true"
  }
}

output "hostname" {
  value = azurerm_app_service.container.default_site_hostname
}