﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;

namespace Tingo.ViewModels
{
    public class CallerCardTile
    { 
    public int Number { get; set; }
    public bool Selected { get; set; }
    public string BackgroundColor { get; set; } = "White";
    public string TextColor { get; set; } = "Black";
    }
}
